#! /bin/sh
[ ! -e Codes.enc ] && touch Codes.enc
cp Codes.enc Codes.enc.backup
openssl enc -bf -pbkdf2 -in Codes.enc -out Codes.2enc -d -a
vim Codes.2enc
openssl enc -bf -pbkdf2 -in Codes.2enc -out Codes.enc -e -a
rm Codes.2enc


